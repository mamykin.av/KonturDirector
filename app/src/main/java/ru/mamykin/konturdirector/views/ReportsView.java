package ru.mamykin.konturdirector.views;

import java.util.List;

import ru.mamykin.konturdirector.models.Period;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:51 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public interface ReportsView {
    void showPeriods(List<Period> periods);

    void showCompanyName(String name);

    void showCompanyStatus(String status);

    void showOkStatusColor();

    void showFailStatusColor();

    void showLoading(boolean show);
}