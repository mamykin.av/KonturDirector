package ru.mamykin.konturdirector.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.mamykin.konturdirector.models.Organization;
import ru.mamykin.konturdirector.models.Period;
import ru.mamykin.konturdirector.models.Report;
import ru.mamykin.konturdirector.models.ReportsResponse;
import ru.mamykin.konturdirector.retrofit.KonturService;
import ru.mamykin.konturdirector.retrofit.ServiceGenerator;
import ru.mamykin.konturdirector.views.ReportsView;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:35 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class ReportsPresenter {
    private static final String ORGANIZATION_EXTRA = "organization";
    private static final String PERIODS_EXTRA = "periods";

    private Organization organization;
    private ArrayList<Period> periodsList;
    private ReportsView view;
    private boolean loading = false;

    public void attachView(ReportsView view) {
        this.view = view;
        if (periodsList == null && !loading) {
            loadCompanyInfo();
        } else {
            showCompanyInfo();
        }
    }

    private void loadCompanyInfo() {
        loading = true;
        view.showLoading(true);

        KonturService service = ServiceGenerator.create(KonturService.class, KonturService.BASE_URL);
        service.getReports().enqueue(new Callback<ReportsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ReportsResponse> call,
                                   @NonNull Response<ReportsResponse> response) {
                ReportsResponse model = response.body();
                if (model != null) {
                    organization = model.getOrganization();
                    periodsList = getPeriods(model.getReportsList());
                    showCompanyInfo();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReportsResponse> call, @NonNull Throwable t) {
                setLoading(false);

                t.printStackTrace();
            }
        });
    }

    private void setLoading(boolean loading) {
        this.loading = loading;
        view.showLoading(loading);
    }

    public void detachView() {
        this.view = null;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(ORGANIZATION_EXTRA, organization);
        outState.putParcelableArrayList(PERIODS_EXTRA, periodsList);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        periodsList = savedInstanceState.getParcelableArrayList(PERIODS_EXTRA);
        organization = savedInstanceState.getParcelable(ORGANIZATION_EXTRA);
    }

    private int getReportMonth(Report report) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss.SSSSSSSZ", Locale.US);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sdf.parse(report.getDate()));
            return calendar.get(Calendar.MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private ArrayList<Period> getPeriods(List<Report> reportsList) {
        SortedMap<Integer, List<Report>> periodsMap = new TreeMap<>();
        ArrayList<Period> periodsList = new ArrayList<>();
        for (Report report : reportsList) {
            int month = getReportMonth(report);
            if (periodsMap.get(month) == null) {
                ArrayList<Report> monthReportsList = new ArrayList<>();
                monthReportsList.add(report);
                periodsMap.put(month, monthReportsList);
            } else {
                periodsMap.get(month).add(report);
            }
        }
        for (Map.Entry<Integer, List<Report>> entry : periodsMap.entrySet()) {
            Period period = new Period();
            period.setMonth(entry.getKey());
            period.setReportsList(entry.getValue());
            periodsList.add(period);
        }
        return periodsList;
    }

    private void showCompanyInfo() {
        setLoading(false);
        if (!isReady())
            return;

        view.showCompanyName(organization.getName());
        view.showPeriods(periodsList);
        view.showCompanyStatus(organization.getStatus().getName());
        if (organization.getStatus().getStatusId() == 0) {
            view.showOkStatusColor();
        } else {
            view.showFailStatusColor();
        }
    }

    private boolean isReady() {
        return periodsList != null && view != null;
    }
}