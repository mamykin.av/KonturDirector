package ru.mamykin.konturdirector.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import ru.mamykin.konturdirector.models.ReportsResponse;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:22 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public interface KonturService {
    String BASE_URL = "http://urdm.ru/";

    @GET("media/img/ob-director.json")
    Call<ReportsResponse> getReports();
}