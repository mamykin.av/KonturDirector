package ru.mamykin.konturdirector.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Creation date: 7/10/2017
 * Creation time: 6:11 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class ServiceGenerator {
    public static <T> T create(Class<T> clazz, String baseUrl) {
        // JSON считаестя Malformed из-за комментариев
        final Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(clazz);
    }
}