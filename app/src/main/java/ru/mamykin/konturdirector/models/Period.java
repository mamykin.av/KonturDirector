package ru.mamykin.konturdirector.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Creation date: 7/10/2017
 * Creation time: 2:17 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Period implements Parcelable {
    private List<Report> reportsList;
    private int month;

    public Period() {
    }

    public Period(List<Report> reportsList, int month) {
        this.reportsList = reportsList;
        this.month = month;
    }

    protected Period(Parcel in) {
        reportsList = in.createTypedArrayList(Report.CREATOR);
        month = in.readInt();
    }

    public static final Creator<Period> CREATOR = new Creator<Period>() {
        @Override
        public Period createFromParcel(Parcel in) {
            return new Period(in);
        }

        @Override
        public Period[] newArray(int size) {
            return new Period[size];
        }
    };

    public List<Report> getReportsList() {
        return reportsList;
    }

    public void setReportsList(List<Report> reportsList) {
        this.reportsList = reportsList;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(reportsList);
        dest.writeInt(month);
    }
}