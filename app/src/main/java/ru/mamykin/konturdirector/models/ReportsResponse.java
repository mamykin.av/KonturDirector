package ru.mamykin.konturdirector.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:18 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class ReportsResponse {
    @SerializedName("organization")
    private Organization organization;
    @SerializedName("events")
    private ArrayList<Report> reportsList;

    public ReportsResponse() {
    }

    public ReportsResponse(Organization organization, ArrayList<Report> eventsList) {
        this.organization = organization;
        this.reportsList = eventsList;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public ArrayList<Report> getReportsList() {
        return reportsList;
    }

    public void setReportsList(ArrayList<Report> reportsList) {
        this.reportsList = reportsList;
    }
}