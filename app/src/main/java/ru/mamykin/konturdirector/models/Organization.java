package ru.mamykin.konturdirector.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:19 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Organization implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("status")
    private OrganizationStatus status;

    public Organization() {
    }

    public Organization(String name, OrganizationStatus status) {
        this.name = name;
        this.status = status;
    }

    protected Organization(Parcel in) {
        name = in.readString();
        status = in.readParcelable(OrganizationStatus.class.getClassLoader());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationStatus getStatus() {
        return status;
    }

    public void setStatus(OrganizationStatus status) {
        this.status = status;
    }

    public static final Creator<Organization> CREATOR = new Creator<Organization>() {
        @Override
        public Organization createFromParcel(Parcel in) {
            return new Organization(in);
        }

        @Override
        public Organization[] newArray(int size) {
            return new Organization[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeParcelable(status, i);
    }

    public static class OrganizationStatus implements Parcelable {
        @SerializedName("name")
        private String name;
        @SerializedName("statusId")
        private int status;

        public OrganizationStatus() {
        }

        public OrganizationStatus(String name, int status) {
            this.name = name;
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatusId() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        protected OrganizationStatus(Parcel in) {
            name = in.readString();
            status = in.readInt();
        }

        public static final Creator<OrganizationStatus> CREATOR = new Creator<OrganizationStatus>() {
            @Override
            public OrganizationStatus createFromParcel(Parcel in) {
                return new OrganizationStatus(in);
            }

            @Override
            public OrganizationStatus[] newArray(int size) {
                return new OrganizationStatus[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(name);
            parcel.writeInt(status);
        }
    }
}