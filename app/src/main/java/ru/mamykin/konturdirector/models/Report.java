package ru.mamykin.konturdirector.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:19 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class Report implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("date")
    private String date;
    @SerializedName("period")
    private String period;
    @SerializedName("status")
    private EventStatus status;

    public Report() {
    }

    public Report(String name, String date, String period, EventStatus status) {
        this.name = name;
        this.date = date;
        this.period = period;
        this.status = status;
    }

    protected Report(Parcel in) {
        name = in.readString();
        date = in.readString();
        period = in.readString();
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public EventStatus getStatus() {
        return status;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(date);
        dest.writeString(period);
    }

    public static class EventStatus implements Parcelable {
        @SerializedName("name")
        private String name;
        @SerializedName("statusId")
        private int status;

        public EventStatus() {
        }

        public EventStatus(String name, int status) {
            this.name = name;
            this.status = status;
        }

        protected EventStatus(Parcel in) {
            name = in.readString();
            status = in.readInt();
        }

        public static final Creator<EventStatus> CREATOR = new Creator<EventStatus>() {
            @Override
            public EventStatus createFromParcel(Parcel in) {
                return new EventStatus(in);
            }

            @Override
            public EventStatus[] newArray(int size) {
                return new EventStatus[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getStatusId() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeInt(status);
        }
    }
}