package ru.mamykin.konturdirector.ui.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.util.List;

import ru.mamykin.konturdirector.R;
import ru.mamykin.konturdirector.models.Period;
import ru.mamykin.konturdirector.presenters.ReportsPresenter;
import ru.mamykin.konturdirector.ui.adapters.PeriodsItemDecorator;
import ru.mamykin.konturdirector.ui.adapters.PeriodsRecyclerAdapter;
import ru.mamykin.konturdirector.views.ReportsView;

public class ReportsActivity extends AppCompatActivity implements ReportsView {
    private ReportsPresenter presenter;
    private PeriodsRecyclerAdapter adapter;
    private TextView tvCompany, tvStatus;
    private AppBarLayout appBarLayout;
    private RecyclerView rvReports;
    private View pbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        presenter = new ReportsPresenter();

        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        tvCompany = (TextView) findViewById(R.id.tvCompany);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        pbLoading = findViewById(R.id.pbLoading);

        rvReports = (RecyclerView) findViewById(R.id.rvReports);
        setupReportsRecyclerView(rvReports);
    }

    private void setupReportsRecyclerView(RecyclerView recyclerView) {
        adapter = new PeriodsRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new PeriodsItemDecorator());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachView(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.detachView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        presenter.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void showPeriods(List<Period> periods) {
        adapter.changeData(periods);
    }

    @Override
    public void showCompanyName(String name) {
        tvCompany.setText(name);
    }

    @Override
    public void showCompanyStatus(String status) {
        tvStatus.setText(status);
    }

    @Override
    public void showOkStatusColor() {
        setStatusBarColor(getColorRes(R.color.colorDarkGreen));
        appBarLayout.setBackgroundColor(getColorRes(R.color.colorLightGreen));
    }

    @Override
    public void showFailStatusColor() {
        setStatusBarColor(getColorRes(R.color.colorDarkRed));
        appBarLayout.setBackgroundColor(getColorRes(R.color.colorLightRed));
    }

    @Override
    public void showLoading(boolean show) {
        pbLoading.setVisibility(show ? View.VISIBLE : View.GONE);
        rvReports.setVisibility(show ? View.GONE : View.VISIBLE);
        appBarLayout.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(color);
        }
    }

    @SuppressWarnings("deprecation")
    private int getColorRes(@ColorRes int color) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                ? getColor(color) : getResources().getColor(color);
    }
}