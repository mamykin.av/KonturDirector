package ru.mamykin.konturdirector.ui.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.mamykin.konturdirector.R;
import ru.mamykin.konturdirector.models.Report;

/**
 * Creation date: 7/10/2017
 * Creation time: 2:15 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class ReportsRecyclerAdapter extends RecyclerView.Adapter
        <ReportsRecyclerAdapter.ReportViewHolder> {
    private List<Report> reportsList;

    public ReportsRecyclerAdapter(List<Report> reportsList) {
        this.reportsList = reportsList;
    }

    @Override
    public ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View contentView = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_report, parent, false);
        return new ReportViewHolder(contentView);
    }

    @Override
    public void onBindViewHolder(ReportViewHolder holder, int position) {
        Report report = reportsList.get(position);
        holder.tvName.setText(report.getName());
        holder.tvPeriod.setText(report.getPeriod());
        holder.tvStatus.setText(report.getStatus().getName());
        int color = ContextCompat.getColor(holder.tvStatus.getContext(),
                report.getStatus().getStatusId() == 2
                        ? R.color.colorLightGreen : R.color.colorLightRed);
        holder.tvStatus.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return reportsList.size();
    }

    class ReportViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPeriod, tvStatus;

        public ReportViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvPeriod = (TextView) itemView.findViewById(R.id.tvPeriod);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
        }
    }
}