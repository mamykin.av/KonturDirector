package ru.mamykin.konturdirector.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.mamykin.konturdirector.R;
import ru.mamykin.konturdirector.models.Period;
import ru.mamykin.konturdirector.models.Report;

/**
 * Creation date: 7/9/2017
 * Creation time: 2:18 PM
 * @author Andrey Mamykin(mamykin_av)
 */
public class PeriodsRecyclerAdapter extends RecyclerView.Adapter
        <PeriodsRecyclerAdapter.PeriodViewHolder> {
    private List<Period> periodsList = new ArrayList<>();

    @Override
    public PeriodsRecyclerAdapter.PeriodViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        final View contentView = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_period, parent, false);
        PeriodViewHolder holder = new PeriodViewHolder(contentView);
        holder.rvReports.addItemDecoration(new ReportsItemDecorator());
        return holder;
    }

    @Override
    public void onBindViewHolder(PeriodsRecyclerAdapter.PeriodViewHolder holder, int position) {
        Period period = periodsList.get(position);
        List<Report> reportsList = period.getReportsList();
        holder.tvPeriod.setText(formatPeriod(holder.tvPeriod.getContext(),
                period.getMonth(), period.getReportsList().size()));
        ReportsRecyclerAdapter adapter = new ReportsRecyclerAdapter(reportsList);
        holder.rvReports.setAdapter(adapter);
    }

    private String formatPeriod(Context context, int month, int count) {
        String monthSuffix = context.getResources().getStringArray(R.array.months_array)[month];
        String passedSuffix = getSuffix(
                count, context.getResources().getStringArray(R.array.pass_array));
        String reportSuffix = getSuffix(count, context.getResources().getStringArray(R.array.report_array));
        return context.getString(
                R.string.section_title_format, monthSuffix, passedSuffix, count, reportSuffix);
    }

    public String getSuffix(int number, String... suffixes) {
        if (suffixes.length != 3) {
            throw new RuntimeException("Неверное число аргументов");
        }
        if (number % 10 == 1) {
            return suffixes[0];
        } else if (number % 10 >= 2 && number <= 4) {
            return suffixes[1];
        }
        return suffixes[2];
    }

    @Override
    public int getItemCount() {
        return periodsList.size();
    }

    public void changeData(@NonNull List<Period> periodsList) {
        this.periodsList = periodsList;
        notifyDataSetChanged();
    }

    class PeriodViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPeriod;
        public RecyclerView rvReports;

        public PeriodViewHolder(View itemView) {
            super(itemView);
            tvPeriod = (TextView) itemView.findViewById(R.id.tvPeriod);
            rvReports = (RecyclerView) itemView.findViewById(R.id.rvReports);
        }
    }
}